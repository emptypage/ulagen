#!/usr/bin/env python3
#
# ulagen.py - Generate Unique Local IPv6 address ::/48 blocks
#
# ref. RFC 4193 - Unique Local IPv6 Unicast Addresses
#   <https://tools.ietf.org/html/rfc4193#section-3.2.1>
#
# Copyright 2019 Masaaki Shibata
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import argparse
import datetime
import hashlib
import ipaddress
import uuid
from calendar import timegm


def ntp_seconds(st):
    utc_ntp_epoch = timegm(datetime.date(1900, 1, 1).timetuple())
    utc_now = timegm(st)
    return utc_now - utc_ntp_epoch


def eui_64(mac_addr):
    hi = ((mac_addr >> 24) * 0x100 + 0xff) ^ 0x02000000
    lo = (mac_addr & 0xffffff) + 0xfe000000
    return (hi << 32) + lo



def main():
    parser = argparse.ArgumentParser(
        description='Generate Unique Local IPv6 address ::/48 blocks')
    args = parser.parse_args()
    ntp_now = ntp_seconds(datetime.datetime.utcnow().timetuple())
    interface_id = eui_64(uuid.getnode())
    key_bytes = '{:016x}{:016x}'.format(ntp_now, interface_id)
    hex_global_id = hashlib.sha1(key_bytes.encode('ascii')).hexdigest()[-10:]
    ipv6_address = ipaddress.IPv6Address(int('fd' + hex_global_id, 16) << 80)
    print(ipv6_address.compressed + '/48')


if __name__ == "__main__":
    main()
